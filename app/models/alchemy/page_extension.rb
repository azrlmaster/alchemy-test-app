#include ApplicationHelper

Alchemy::Page.module_exec do
  after_save :update_sidebar_links, if: -> { public && !read_attribute(:published_at).nil? }, unless: :systempage? || read_attribute(:name) == 'index'
  
  def update_sidebar_links
  
    new_page = Alchemy::Page.newest_addition
    
    return nil if Rails.application.config.recent_pages[0] == new_page
    
    Rails.application.config.recent_pages.unshift(new_page)
    Rails.application.config.recent_pages.delete_at(4)
  
  end
  
  def self.newest_addition
    puts "Step 0"
    page = Alchemy::Page.order(published_at: :desc).limit(1)
    puts "step 1"
    element = Alchemy::Element.where("page_id = ? AND name = ?", page[0][:id], "blog_title").select("id")
    puts "step 2"
    element_id = element[0][:id]
    puts "step 3"
    essences = Alchemy::Content.where("element_id = ?", element_id)
    puts "step 4"

    content = []
    2.times do |i|
      content.push(Alchemy::EssenceText.where("id = ?", essences[i][:essence_id]))
    end
    body = []
    2.times do |i|
      body.push(content[i][0][:body])
    end
    
    body.push(page[0][:published_at])
    body.push(page[0][:urlname])
    return body
  end
  
  def test123
    puts "test"
  end
  
end