Alchemy::PagesController.module_exec do
 # before_action :archive_page, only: :show
  
  def index
    render :layout => false
    
    @page || page_not_found!

    if Alchemy::Config.get(:redirect_index)
      redirect_permanently_to page_redirect_url
    else
      authorize! :index, @page
      render_page if render_fresh_page?
    end
  end
#  def archive_page
#    @pages = Alchemy::Page.all
#  end
  
end