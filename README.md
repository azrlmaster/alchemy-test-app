# Bryan's Website App
This is an application for learning how to work in and with the AlchemyCMS content management system.

##### Purpose
As I'm just getting back into coding, and much moreso than in the past, it is easiest for me to learn by doing. The end-game of this application is to complete the remaining tasks I have, and then fully re-write all the necessary code. 

At that point, I'll have my functional template that I can deploy to my new sites and have the features pre-built that I'm specifically looking for.

##### Tasks To Do
* Finish exploring the databases that Alchemy uses
    * Add to the database to make queries easier
    * Build better method of keeping track of recent pages
* Build out static pages
    * Try to reduce the overall number of elements in the site by reusing elements more efficiently
* Design dynamic "section" pages
    * IE a main projects page that automatically populates the correct information / previews / whatever.

##### Setup
I don't really have this available as a project for other to use, but if you're interested in giving it a whirl, have at it.

There are a few locations that things need to be manually changed once you pull the repository, initialize the postgres server, and then the Rails server.

1. Once you create a new language tree, make sure to update the URL for the 'index' page to 'home'
2. The static pages must be named properly to have the header links work at this time. Either call them logically (contact, about, etc) or re-write the links in the header.

##### Resources
* My development is all done on [ Cloud 9 ](c9.io)
* Of course this project uses [AlchemyCMS](alchemy-cms.com)
* My initial knowledge of Ruby was gained [ The Hard Way ](learncodethehardway.com)
* I then started working with the [ Rails Tutorial ](railstutorial.org)